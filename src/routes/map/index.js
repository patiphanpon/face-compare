/**
 * Created by Administrator on 2017/1/5.
 */

import login from './modules/login';

export default [
    ...login,
    {
        path: '/',
        redirect: '/login'
    },
    {
        path: '*',
        redirect: '/'
    },
]
